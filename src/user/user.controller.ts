import { Controller, Get, Patch, Req, UseGuards } from '@nestjs/common';
import { JwtGuard } from 'src/auth/guard';
import { GetUser } from '../auth/decorator'
import { User } from '@prisma/client';

@Controller('users')
export class UserController {

    @UseGuards(JwtGuard)
    @Get('me')
    getMe(
        @GetUser() user: User
        ) {
          
    // console.log({
    //     user: req.user
    // })
        return user;
    }

    @Patch()
    editUser() {

    }
}
