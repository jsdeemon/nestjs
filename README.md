### Nest Rest API with prisma ORM 

```bash
$ sudo docker-compsoe up -d 
$ npx prisma migrate deploy 
$ yarn start:dev 
```

https://www.youtube.com/watch?v=GHTA143_b-s 

$ npm i -g @nestjs/cli
$ nest new project-name 

sudo npm i -g @nestjs/cli

nest new nestjs-api-tutorial 


Decirator is a function that adds a metadata to a class 

Every modulce can import itself controllers, and providers 

yarn start:dev 

generate new module 

nest g module user  

nest g module bookmark 


PROVIDERS are responsible for business logic 

We separate the logic to controllers and services to be able to simplify what we do 

  
dependency injection 

POST  http://localhost:3333/auth/signup 

Nestjs automatically convert datatype de[ending on data we return 

sudo docker ps

sudo docker-compose up -d 

sudo docker ps
sudo docker logs containerID 


Prisma ORM 
https://www.prisma.io/ 

yarn add -D prisma 

yarn add @prisma/client  

npx prisma init 

npx prisma --help  

npx prisma migrate dev 


npx prisma studio  


nest g module prisma 

nest g service prisma --no-spec 


DTO - data transfer object

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
